package upload

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import bootstrap._
import assertions._
import scala.concurrent.forkjoin.ThreadLocalRandom
import java.io.{FileWriter, BufferedWriter}

class ArtifactoryUploadSimulation extends Simulation {

  val httpProtocol = http
    .baseURL("http://localhost:8081/artifactory")
    .acceptCharsetHeader("ISO-8859-1,utf-8;q=0.7,*;q=0.7")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .acceptEncodingHeader("gzip, deflate")
    .disableFollowRedirect

  val headers_3 = Map(
    "Keep-Alive" -> "115",
    "Content-Type" -> "application/x-www-form-urlencoded")

  val filesFeeder = new Feeder[String] {
    import java.io.File

    // always return true as this feeder can be polled infinitely
    override def hasNext = true

    def recursiveScanFiles(f: File): Array[File] = {
      val files = f.listFiles
      files ++ files.filter(_.isDirectory).flatMap(recursiveScanFiles)
    }

    val root = new File("C:\\Work\\.m2\\repository")
    val files = recursiveScanFiles(root).filter(!_.isDirectory).filter(!_.getName.endsWith(".sha1")).filter(!_.getName.endsWith(".md5"))

    val filePaths = files map { f => (f.getAbsolutePath,
      "/ext-release-local".concat(f.getAbsolutePath.substring(root.getAbsolutePath.length).replaceAll("\\\\", "/"))) } toMap

    //println("Files found: " + filePaths.mkString("\n"))
    private val writer: BufferedWriter = new BufferedWriter(new FileWriter(new File("target/files.csv")))
    writer.write("filePath,repoPath\n")
    writer.write(filePaths.mkString("\n"))
    writer.close()

    override def next: Map[String, String] = {
      //val file = files(randInt(0, files.size - 1))
      val file = files(ThreadLocalRandom.current.nextInt(files.size - 1))

      Map("filePath" -> file.getAbsolutePath,
          "repoPath" -> "/ext-release-local/".concat(file.getAbsolutePath.substring(root.getAbsolutePath.length).replaceAll("\\\\", "/"))
      )
    }
  }

  val scn = scenario("Concurrent Upload Scenario")
    .feed(csv("user_information.csv").circular)
    .feed(csv("files.csv").random)
    //.feed(filesFeeder)
    .repeat(300) {
      exec(
        http("${repoPath}")
          .put("${repoPath}")
          .basicAuth("${username}", "${password}")
          .body(RawFileBody("${filePath}"))
          .check(status.is(201))
      )
    }

  setUp(scn.inject(ramp(40 users) over (10 seconds)))
    .protocols(httpProtocol)
    .assertions(
    global.successfulRequests.percent.is(100), details("Login" / "request_2").responseTime.max.lessThan(2000),
    details("request_9").requestsPerSec.greaterThan(10))
}
